import React, { ReactNode, useState, useEffect, useContext } from "react";
import { auth } from "../firebase";

interface UserContext {
  user: firebase.User | null;
  login: (
    email: string,
    password: string
  ) => Promise<firebase.auth.UserCredential>;
  signUp: (
    email: string,
    password: string
  ) => Promise<firebase.auth.UserCredential>;
  signOut: () => Promise<void>;
}
export const UserContext = React.createContext<UserContext>({
  user: null,
  login: () => Promise.resolve({} as firebase.auth.UserCredential),
  signUp: () => Promise.resolve({} as firebase.auth.UserCredential),
  signOut: () => Promise.resolve()
});

type Props = {
  children: ReactNode;
};

export default function UserProvider({ children }: Props) {
  const [user, setUser] = useState<firebase.User | null>(null);

  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged((userAuth) => {
      setUser(userAuth);
    });
    return () => unsubscribe();
  }, []);

  const login = (email: string, password: string) => {
    return auth.signInWithEmailAndPassword(email, password);
  };

  const signUp = (email: string, password: string) => {
    return auth.createUserWithEmailAndPassword(email, password).then(() => {
      return login(email, password);
    });
  };

  const signOut = () => {
    return auth.signOut();
  };

  return (
    <UserContext.Provider value={{ user, login, signUp, signOut }}>
      {children}
    </UserContext.Provider>
  );
}

export function useUser() {
  return useContext(UserContext);
}
