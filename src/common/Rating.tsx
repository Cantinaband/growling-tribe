import React from "react";
import styled from "styled-components";

const Container = styled.div`
  display: flex;
  flex-wrap: nowrap;
  height: 32px;
  align-items: center;
  justify-content: flex-end;
`;

const Star = styled.svg`
  width: 20px;
  padding: 4px;
`;
type Props = {
  rating?: number;
  max: number;
  onRateChange?: (value: number) => void;
};

export default function Rating({ rating = 0, max, onRateChange }: Props) {
  const getOffset = (index: number) => {
    const int = Math.trunc(rating);
    const float = rating - int;

    if (index < int) {
      return 1;
    } else if (index > int) {
      return 0;
    }
    return float;
  };
  return (
    <Container>
      {Array.from("_".repeat(max)).map((_, i) => (
        <Star
          key={i}
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 19 18"
          onClick={() => onRateChange && onRateChange(i + 1)}
        >
          <path
            fill={
              Number.isInteger(rating)
                ? i < rating
                  ? "black"
                  : "transparent"
                : `url(#progress_${i})`
            }
            stroke="#000"
            strokeLinejoin="round"
            strokeWidth="2"
            d="M10.055 14.104a1 1 0 00-1.057 0l-4.41 2.743 1.248-5.004a1 1 0 00-.323-1.005L1.52 7.454l5.118-.356a1 1 0 00.854-.613l2.033-4.883 2.034 4.883a1 1 0 00.854.613l5.117.356-3.991 3.385a1 1 0 00-.324 1.005l1.248 5.004-4.41-2.743z"
          />
          <defs>
            <linearGradient id={`progress_${i}`} x1="0" y1="0" x2="1" y2="0">
              <stop id="stop1" offset={getOffset(i)} stopColor="#000" />
              <stop id="stop2" offset={getOffset(i)} stopColor="transparent" />
            </linearGradient>
          </defs>
        </Star>
      ))}
    </Container>
  );
}
