import styled, { css } from "styled-components";

export const buttonStyle = css`
  padding: 16px;
  font-size: 16px;
  text-align: center;
  border-radius: 8px;
  text-decoration: none;
  background: #000;
  color: #fff;
  font-weight: bold;
  :disabled {
    opacity: 0.2;
  }
`;
export const smallButtonStyle = css`
  ${buttonStyle}
  padding: 8px 16px;
  font-size: 12px;
`;

const Button = styled.button<{ small?: boolean }>`
  ${buttonStyle}
  ${(p) => p.small && smallButtonStyle}
`;

export default Button;
