import styled from "styled-components";
import React from "react";

const Container = styled.div`
  width: 2rem;
  height: 2rem;
  display: flex;
  align-items: center;
  justify-content: center;
  background: #fff;
  box-shadow: 0px 2px 12px rgba(0, 0, 0, 0.12);
  border-radius: 1rem;
  padding: 8px;
  box-sizing: border-box;
`;
type Props = {
  className?: string;
  onClick?: () => void;
};
export default function RoundClose({ className, onClick }: Props) {
  return (
    <Container className={className} onClick={onClick}>
      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
        <path
          stroke="#000"
          strokeLinecap="round"
          strokeWidth="2.5"
          d="M2 22.232L22.232 2M1.768 2L22 22.232"
        />
      </svg>
    </Container>
  );
}
