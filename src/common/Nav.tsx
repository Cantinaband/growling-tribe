import styled from "styled-components";
import React, { ReactNode } from "react";
import { useHistory } from "react-router-dom";

const Navigation = styled.nav`
  display: flex;
  flex-wrap: nowrap;
  align-items: center;
  padding: 16px;
  z-index: 999;
  box-shadow: 0px 2px 12px rgba(0, 0, 0, 0.12);
`;

const BackButton = styled.svg`
  padding: 8px;
  height: 24px;
  width: 24px;
  transform: rotate(180deg);
`;

type Props = {
  children?: ReactNode;
};
export default function Nav({ children }: Props) {
  const history = useHistory();
  return (
    <Navigation>
      <BackButton
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 18 25"
        fill="none"
        onClick={() => history.goBack()}
      >
        <path
          stroke="#000"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="2.5"
          d="M2 2l14 10.5L2 23"
        />
      </BackButton>
      {children}
    </Navigation>
  );
}
