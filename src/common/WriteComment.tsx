import TextArea from "./TextArea";
import styled from "styled-components";
import React, { useState, useRef } from "react";
import Rating from "./Rating";
import Button, { smallButtonStyle } from "./Button";
import { saveComment } from "../firebase";
import { useUser } from "../provider/UserProvider";
import { Link } from "react-router-dom";
import { MarkerDetails } from "../typings/marker";

const TRANSITION_DURATION = "400ms";

const Container = styled.div`
  max-height: 0;
  background: #f6f6f6;
  transition: all 400ms ease-in-out;
  overflow: hidden;
`;

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 8px 0;
  font-weight: bold;
  color: #000;
  border-bottom: 2px solid #000;
`;

const Arrow = styled.svg<{ expanded: boolean }>`
  width: 16px;
  transition: transform ${TRANSITION_DURATION} ease-in-out;
  transform: rotate(${(p) => (p.expanded ? 180 : 0)}deg);
`;

const Content = styled.div`
  padding: 16px;
`;

const StyledButton = styled(Button)`
  display: block;
  margin: 16px 0 0 auto;
`;

const LoginLink = styled(Link)`
  ${smallButtonStyle}
`;

type Props = {
  markerDetails: MarkerDetails;
};
export default function WriteComment({ markerDetails }: Props) {
  const [expanded, setExpanded] = useState(false);
  const [pending, setPending] = useState(false);
  const [rating, setRating] = useState(0);
  const [comment, setComment] = useState("");
  const containerRef = useRef<HTMLDivElement>(null);
  const textAreaRef = useRef<HTMLTextAreaElement>(null);
  const height = useRef(0);
  const { user } = useUser();
  const hasRated = markerDetails.comments?.some((c) => c.userId === user?.uid);
  const handleSave = () => {
    setPending(true);
    saveComment(rating, comment, markerDetails, user!.uid).then(() => {
      setPending(false);
      setRating(0);
      if (textAreaRef.current) {
        textAreaRef.current.value = "";
      }
      setExpanded(false);
    });
  };
  return (
    <div>
      <Header
        onClick={() => {
          if (containerRef.current) {
            height.current = containerRef.current.scrollHeight;
          }
          setExpanded((prev) => !prev);
        }}
      >
        {user ? (
          hasRated ? (
            <>you rated this place already :)</>
          ) : (
            <>
              rate & leave a comment
              <Arrow
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 20 14"
                expanded={expanded}
              >
                <path
                  stroke="#000"
                  strokeLinejoin="round"
                  strokeWidth="2.5"
                  d="M18 2H2l8.381 10L18 2z"
                />
              </Arrow>
            </>
          )
        ) : (
          <>
            login to rate this place
            <LoginLink to="/login">login</LoginLink>
          </>
        )}
      </Header>
      {user && (
        <Container
          style={{
            maxHeight: expanded ? height.current : 0,
            opacity: pending ? 0.5 : 1,
            pointerEvents: pending ? "none" : "all"
          }}
          ref={containerRef}
        >
          <Content>
            <Rating
              max={5}
              onRateChange={(value) => {
                setRating(value);
              }}
              rating={rating}
            />
            <TextArea
              ref={textAreaRef}
              onChange={(e) => setComment(e)}
              placeholder="write something about this place :)"
            />
            <StyledButton small onClick={handleSave}>
              save
            </StyledButton>
          </Content>
        </Container>
      )}
    </div>
  );
}
