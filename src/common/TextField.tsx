import styled from "styled-components";
import React from "react";

const Container = styled.div`
  position: relative;
  width: 100%;
  margin-top: 20px;
`;
const Input = styled.input<{ ref: any }>`
  font-size: 20px;
  width: 100%;
  outline: none;
  border: none;
  border-bottom: 2px solid rgba(0, 0, 0, 0.5);
  margin-bottom: 16px;
  padding: 4px 0;
  border-radius: 0;
  :not(:placeholder-shown) + label,
  :focus + label {
    transform: translateY(calc(-100% - 4px)) scale(0.7);
  }
  :focus,
  :not(:placeholder-shown) {
    border-bottom: 2px solid rgba(0, 0, 0, 1);
  }
  :-webkit-autofill,
  :-webkit-autofill:hover,
  :-webkit-autofill:focus,
  :-webkit-autofill:active {
    box-shadow: 0 0 0 30px white inset !important;
  }
`;

const Label = styled.label`
  position: absolute;
  font-size: 20px;
  top: 4px;
  left: 0;
  color: rgba(0, 0, 0, 0.5);
  transition: all 200ms ease-in-out;
  pointer-events: none;
  transform-origin: 0 center;
`;

type Props = {
  type: "email" | "password" | "text";
  placeholder: string;
  name: string;
};

const TextField = React.forwardRef(
  ({ placeholder, name, type }: Props, ref) => {
    return (
      <Container>
        <Input type={type} name={name} ref={ref} placeholder=" " />
        <Label>{placeholder}</Label>
      </Container>
    );
  }
);
export default TextField;
