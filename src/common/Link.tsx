import styled from "styled-components";
import { Link as RouterLink } from "react-router-dom";

const Link = styled(RouterLink)`
  font-size: 16px;
  color: #000;
`;

export default Link;
