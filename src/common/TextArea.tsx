import styled from "styled-components";
import React from "react";

const Container = styled.div`
  position: relative;
  margin-top: 20px;
`;

const Area = styled.textarea<{ ref: any }>`
  box-sizing: border-box;
  border: none;
  outline: none;
  border-bottom: 2px solid #000;
  font-size: 20px;
  resize: none;
  width: 100%;
  background: inherit;
  border-radius: 0;
  :not(:placeholder-shown) + label,
  :focus + label {
    transform: translateY(calc(-100% - 4px));
    font-size: 16px;
  }
  :focus,
  :not(:placeholder-shown) {
    border-bottom: 2px solid rgba(0, 0, 0, 1);
  }
  :-webkit-autofill,
  :-webkit-autofill:hover,
  :-webkit-autofill:focus,
  :-webkit-autofill:active {
    box-shadow: 0 0 0 30px inherit inset !important;
  }
`;

const Label = styled.label`
  position: absolute;
  font-size: 20px;
  top: 4px;
  left: 0;
  color: rgba(0, 0, 0, 0.5);
  transition: all 200ms ease-in-out;
  pointer-events: none;
`;

type Props = {
  placeholder?: string;
  name?: string;
  className?: string;
  onChange?: (value: string) => void;
};

const TextArea = React.forwardRef(
  ({ placeholder, name, className, onChange }: Props, ref) => {
    return (
      <Container className={className}>
        <Area
          ref={ref}
          rows={3}
          placeholder=" "
          name={name}
          onChange={(e) => onChange && onChange(e.target.value)}
        />
        <Label>{placeholder}</Label>
      </Container>
    );
  }
);

export default TextArea;
