import React, { ReactNode } from "react";
import styled from "styled-components";

const Slider = styled.div<{ padding: string; ref: any }>`
  display: flex;
  overflow-y: scroll;
  overflow-x: hidden;
  box-sizing: border-box;
  padding: ${(p) => p.padding};
  width: 100%;
  -webkit-overflow-scrolling: touch;
  ::-webkit-scrollbar {
    display: none;
  }
  *:nth-last-child(1) {
    margin: 0;
  }
  :after {
    content: "";
    display: flex;
    height: 1px;
    min-width: ${(p) => lastItemMargin(p.padding)};
  }
`;

function lastItemMargin(padding: string) {
  const pads = padding.split(" ");

  switch (pads.length) {
    case 1:
      return pads[0];
    case 2:
      return pads[1];
    case 3:
      return pads[1];
    case 4:
      return pads[2];
  }
}

type Props = {
  padding: string;
  children: ReactNode;
  className?: string;
  onTouchStart?: any;
  onTouchMove?: any;
  onTouchEnd?: any;
};
const HorizontalSlider = React.forwardRef(
  (
    {
      padding,
      children,
      className,
      onTouchEnd,
      onTouchMove,
      onTouchStart
    }: Props,
    ref
  ) => (
    <Slider
      padding={padding}
      className={className}
      ref={ref}
      onTouchStart={onTouchStart}
      onTouchMove={onTouchMove}
      onTouchEnd={onTouchEnd}
    >
      {children}
    </Slider>
  )
);

export default HorizontalSlider;
