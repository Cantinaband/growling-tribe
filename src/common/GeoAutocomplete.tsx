import React, { useState, useEffect, useRef } from "react";
import SearchBar from "./SearchBar";
import styled from "styled-components";
import { useMap } from "../Map/MapProvider";
import useOnClickOutside from "../hooks/useOnClickOutside";

export type SearchResult = {
  center: [number, number];
  placeName: string;
  secondaryPlaceName: string;
};

const Container = styled.div`
  flex: 1 1 0%;
  position: relative;
  height: 40px;
`;

const Results = styled.div`
  position: absolute;
  top: 100%;
  left: 0;
  right: 0;
  display: grid;
  grid-template-columns: 1fr;
  grid-column-gap: 0px;
  grid-row-gap: 0px;
  overflow: hidden;
  background: #fff;
  box-shadow: 0px 12px 12px 0px rgba(0, 0, 0, 0.12);
`;
const Result = styled.div`
  padding: 8px 16px;
  div:nth-child(1) {
    font-size: 16px;
    font-weight: bold;
  }
  div:nth-child(2) {
    font-size: 12px;
    color: rgba(0, 0, 0, 0.3);
  }
`;

export default function GeoAutocomplete() {
  const [search, setSearch] = useState("");
  const [results, setResults] = useState<SearchResult[]>([]);
  const resultsRef = useRef<HTMLDivElement>(null);
  useOnClickOutside(resultsRef, () => {
    setResults([]);
  });

  useEffect(() => {
    if (search.length > 2) {
      fetch(
        `https://api.mapbox.com/geocoding/v5/mapbox.places/${search
          .split(" ")
          .join("%20")}.json?access_token=${
          process.env.REACT_APP_MAPBOX_ACCESS_TOKEN
        }`
      )
        .then((response) => response.json())
        .then((data) => {
          const { features } = data;
          const filteredResults = features.slice(0, 4).map((f: any) => ({
            center: f.center,
            placeName: f.place_name.split(",")[0],
            secondaryPlaceName: f.place_name
              .split(",")
              .slice(1)
              .join(",")
              .trim()
          })) as SearchResult[];
          setResults(filteredResults);
        });
    } else {
      setResults([]);
    }
  }, [search, search.length]);
  const { setSearchResult } = useMap();
  return (
    <Container>
      <SearchBar onChange={(value) => setSearch(value)} />
      <Results ref={resultsRef}>
        {results.map((r, i) => (
          <Result
            onClick={() => {
              setSearchResult(r);
              setResults([]);
            }}
            key={i}
          >
            <div>{r.placeName}</div>
            <div>{r.secondaryPlaceName}</div>
          </Result>
        ))}
      </Results>
    </Container>
  );
}
