import React from "react";
import styled from "styled-components";

const Container = styled.div`
  display: flex;
  flex: 1 1 0%;
  justify-content: flex-end;
  height: 100%;
`;

const Input = styled.input`
  position: relative;
  border: none;
  outline: none;
  padding: 0 0 0 8px;
  margin: 0 8px;
  width: 32px;
  font-size: 16px;
  background-image: url("data:image/svg+xml,%3Csvg width='24' height='24' viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath fill-rule='evenodd' clip-rule='evenodd' d='M13.2857 19.4286C17.5462 19.4286 21 15.9748 21 11.7143C21 7.4538 17.5462 4 13.2857 4C9.02521 4 5.57141 7.4538 5.57141 11.7143C5.57141 15.9748 9.02521 19.4286 13.2857 19.4286Z' stroke='black' stroke-width='2.5'/%3E%3Cpath d='M7.28571 16.8571L3 20.2857' stroke='black' stroke-width='2.5'/%3E%3C/svg%3E%0A");
  background-repeat: no-repeat;
  background-position: right 8px center;
  background-size: 24px 24px;
  border-radius: 12px;
  transition: width 400ms 0s, background-color 400ms 400ms;
  ::-webkit-input-placeholder {
    opacity: 0;
    transition: opacity 200ms ease-in;
  }
  :focus,
   :not(:placeholder-shown) {
    transition: width 400ms 0s, background-color 400ms 0ms;
    background-color: #f6f6f6;
    padding: 0 40px 0 16px;
    width: 100%;
    ::-webkit-input-placeholder {
      opacity: 1;
      transition-delay: 300ms;
    }
  }
`;
type Props = {
  onChange: (value: string) => void;
};

export default function SearchBar({ onChange }: Props) {
  return (
    <Container>
      <Input
        type="text"
        placeholder="search some place..."
        onChange={(e) => onChange(e.target.value)}
      />
    </Container>
  );
}
