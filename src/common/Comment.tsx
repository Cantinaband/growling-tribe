import styled from "styled-components";
import React from "react";
import { Comment as CommentProp } from "../typings/marker";
import Rating from "./Rating";

const Container = styled.div`
  padding: 16px;
  border: 2px solid rgba(0, 0, 0, 0.12);
  border-radius: 8px;
  overflow-wrap: break-word;
  word-wrap: break-word;
`;

const Row = styled.div`
  display: flex;
  flex-wrap: nowrap;
  margin-top: 16px;
  align-items: center;
`;

type Props = {
  comment: CommentProp;
};

export default function Comment({ comment }: Props) {
  return (
    <Container>
      {comment.message}
      <Row>
        <Rating max={5} rating={comment.rating} />
      </Row>
    </Container>
  );
}
