import React, { useState, useRef } from "react";
import styled, { css } from "styled-components";
import { MenuDots, User, Heart, Add } from "../common/Icon";
import { useHistory } from "react-router-dom";
import useOnClickOutside from "../hooks/useOnClickOutside";

const expandedStyle = css`
  div:nth-child(2) {
    transition-delay: 200ms;
    transform: translate3d(-50%, -150%, 0) scale(1);
  }
  div:nth-child(3) {
    transition-delay: 100ms;
    transform: translate3d(-150%, 0%, 0) scale(1);
  }
  div:nth-child(4) {
    transform: translate3d(-50%, 150%, 0) scale(1);
  }
`;
const Container = styled.div<{ expanded: boolean }>`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  ${(p) => p.expanded && expandedStyle}
`;

const MainIcon = styled.div`
  width: 3rem;
  height: 3rem;
  box-sizing: border-box;
  z-index: 1;
  padding: 5px;
  border-radius: 100%;
  background: #fff;
  box-shadow: 0px 2px 12px rgba(0, 0, 0, 0.12);
`;

const FloatingIcon = styled.div`
  box-shadow: 0px 2px 12px rgba(0, 0, 0, 0.12);
  box-sizing: border-box;
  padding: 6px;
  position: absolute;
  transition: all 200ms ease-in-out;
  width: 2rem;
  height: 2rem;
  background: #fff;
  border-radius: 100%;
`;

type Props = {
  className?: string;
};
export default function FloatingMenu({ className }: Props) {
  const [expanded, setExpanded] = useState(false);
  const history = useHistory();
  const containerRef = useRef<HTMLDivElement>(null);

  useOnClickOutside(containerRef, () => setExpanded(false));

  return (
    <Container className={className} expanded={expanded} ref={containerRef}>
      <MainIcon onClick={() => setExpanded(!expanded)}>
        <MenuDots />
      </MainIcon>
      <FloatingIcon onClick={() => history.push("/add")}>
        <Add />
      </FloatingIcon>
      <FloatingIcon onClick={() => history.push("/user")}>
        <User />
      </FloatingIcon>
      <FloatingIcon>
        <Heart />
      </FloatingIcon>
    </Container>
  );
}
