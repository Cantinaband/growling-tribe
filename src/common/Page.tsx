import { ReactNode } from "react";
import React from "react";
import styled from "styled-components";
import Nav from "./Nav";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  background: #fff;
`;

const Content = styled.div<{ padded?: boolean }>`
  display: flex;
  position: relative;
  flex-direction: column;
  height: 100%;
  padding: ${(p) => (p.padded ? "16px" : 0)};
  overflow-y: scroll;
`;

type Props = {
  children: ReactNode;
  padded?: boolean;
  hideNav?: boolean;
};

export default function Page({ children, padded }: Props) {
  return (
    <Container>
      <Nav />
      <Content padded={padded}>{children}</Content>
    </Container>
  );
}
