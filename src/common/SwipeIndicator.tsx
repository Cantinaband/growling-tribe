import styled from "styled-components";

const SwipeIndicator = styled.div`
  margin: 0 auto 16px;
  background: #000;
  width: 56px;
  height: 5px;
  border-radius: 3px;
`;
export default SwipeIndicator;
