import * as firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

import { AddMarker, MarkerDetails, Filter } from "../typings/marker";

const firebaseConfig = {
  apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
  authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
  projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
  storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET
};

firebase.initializeApp(firebaseConfig);
firebase.firestore().enablePersistence();

export const auth = firebase.auth();
const db = firebase.firestore();

export const getFilters = () => {
  return db.collection("Filters");
};

export const addFilter = async (filter: string) => {
  return await db.collection("Filters").add({ filter });
};

export const getMarkers = (filters?: Filter[]) => {
  if (filters) {
    return db
      .collection("Markers")
      .where("category", "array-contains-any", filters);
  }
  return db.collection("Markers");
};

export const addMarker = async (marker: AddMarker) => {
  const { images, description, category, geopoint } = marker;

  const imageURLs = images.map(async (i) => {
    const storage = firebase.storage().ref().child(`images/${i.name}`);
    return (await storage
      .put(i)
      .then(() =>
        storage.getDownloadURL().then((url: string) => url)
      )) as string;
  });

  return Promise.all(imageURLs).then((e) =>
    db
      .collection("MarkerDetails")
      .add({
        images: e,
        description,
        category,
        geopoint
      })
      .then((e) => {
        db.collection("Markers").add({
          detailsRef: e,
          geopoint,
          category
        });
      })
  );
};

export const saveComment = async (
  rating: number,
  comment: string,
  markerDetails: MarkerDetails,
  userId: string
) => {
  let newRatingValue = rating;

  if (markerDetails.rating) {
    const overAll = markerDetails.rating.value;
    const totalRatings = markerDetails.rating.numberOfRatings;
    const newRating = rating;
    const newtotalRatings = markerDetails.rating?.numberOfRatings + 1;
    newRatingValue = (overAll * totalRatings + newRating) / newtotalRatings;
  }
  const newRatingsNumber = markerDetails.rating
    ? markerDetails.rating?.numberOfRatings + 1
    : 1;

  return await db
    .collection("MarkerDetails")
    .doc(markerDetails.id)
    .update({
      comments: firebase.firestore.FieldValue.arrayUnion({
        rating,
        message: comment,
        createdAt: firebase.firestore.Timestamp.now(),
        userId
      }),
      rating: {
        value: newRatingValue,
        numberOfRatings: newRatingsNumber
      }
    });
};
