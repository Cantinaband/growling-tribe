import React, { useState, useEffect, RefObject, useRef, useMemo } from "react";
import styled, { css } from "styled-components";
import HorizontalSlider from "../common/HorizontalSlider";
import { interpolateRange } from "../utils";
import SwipeIndicator from "../common/SwipeIndicator";
import Rating from "../common/Rating";
import Comment from "../common/Comment";
import WriteComment from "../common/WriteComment";
import { useMap } from "./MapProvider";
import useMarkerDetails from "../hooks/useMarkerDetails";

export const SLIDER_DURATION = 300;
const TOUCH_THRESHOLD = 10;
const GESTURE_THRESHOLD = 30;

export enum SliderState {
  CLOSE,
  SMALL,
  MEDIUM,
  OPEN
}

type ContainerProps = {
  animated: boolean;
  animationDuration: number;
  mapSliderState: SliderState;
};

const Container = styled.div<ContainerProps>`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 4;
  height: 100%;
  width: 100%;
  touch-action: none;
  pointer-events: none;
  overflow-y: hidden;
  transform: translate3d(0, 70%, 0);
  ${(p) =>
    p.mapSliderState === SliderState.OPEN &&
    p.animated &&
    css`
      overflow-y: scroll;
      pointer-events: all;
      touch-action: auto;
    `};
  ${(p) =>
    p.animated &&
    css`
      transition: transform ${p.animationDuration}ms ease-out;
    `}
`;

const SliderImage = styled.img`
  width: 65vw;
  object-fit: cover;
  margin-right: 4px;
  height: 100%;
`;

type SliderProps = {
  sliderState: SliderState;
  animated: boolean;
  animationDuration: number;
};

const ImageSlider = styled(HorizontalSlider)<SliderProps>`
  position: relative;
  height: 30%;
  pointer-events: all;
  touch-action: auto;
  transform: translateY(50%);
  background: #fff;
  border-radius: 8px 8px 0 0;
  box-shadow: 0px -2px 12px rgba(0, 0, 0, 0.12);
  ${(p) =>
    p.sliderState === SliderState.OPEN &&
    css`
      border-radius: 0;
    `}
  ${(p) =>
    p.animated &&
    css`
      transition: transform ${p.animationDuration}ms ease-out;
    `};
`;

const ContentContainer = styled.div`
  position: relative;
  min-height: 70%;
  transform: translate3d(0, 0, 0);
  background: #fff;
  padding: 0 16px 16px 16px;
  box-sizing: border-box;
  pointer-events: all;
`;

const ContentHeader = styled.div<{ ref: RefObject<HTMLDivElement> }>`
  padding-top: 16px;
  padding-bottom: 8px;
`;

const Comments = styled.div`
  display: flex;
  flex-direction: column;
  & > * {
    margin: 0;
  }
  & > * + * {
    margin-top: 16px;
  }
`;

const NoComment = styled.h1`
  padding: 32px;
  text-align: center;
  color: rgba(0, 0, 0, 0.3);
  background: #f6f6f6;
`;

type Props = {
  menuRef: RefObject<HTMLDivElement>;
  locateRef: RefObject<HTMLDivElement>;
};

const MemoRating = React.memo(Rating);
const MemoSwipeIndicator = React.memo(SwipeIndicator);

export default function MapSlider({ menuRef, locateRef }: Props) {
  const [animated, setAnimated] = useState(true);
  const startY = useRef(0);
  const [moveY, setMoveY] = useState(0);
  const scrollTopOnMoveStart = useRef<number>(0);
  const sliderRef = useRef<HTMLDivElement>(null);
  const imageRef = useRef<HTMLDivElement>(null);
  const contentHeaderRef = useRef<HTMLDivElement>(null);
  const HEIGHT = window.innerHeight;
  const contentHeaderHeight = useRef(HEIGHT * 0.1);
  const scrollTimer = useRef<null | number>(null);
  const blockSlider = useRef(false);
  const animationDuration = useRef(SLIDER_DURATION);
  const { mapSliderState, setMapSliderState, selectedMarker } = useMap();
  const { markerDetails } = useMarkerDetails(selectedMarker);

  const CommentsMemo = useMemo(
    () =>
      markerDetails && (
        <Comments>
          <WriteComment markerDetails={markerDetails} />
          {markerDetails.comments ? (
            markerDetails.comments
              .sort((a, b) => b.createdAt.toMillis() - a.createdAt.toMillis())
              .map((c, i) => <Comment comment={c} key={i} />)
          ) : (
            <NoComment>no comments :(</NoComment>
          )}
        </Comments>
      ),
    [markerDetails]
  );

  //detect image gallery scroll end
  useEffect(() => {
    const ref = imageRef.current;
    if (ref) {
      const handleScroll = () => {
        if (scrollTimer.current !== null) {
          blockSlider.current = true;
          clearTimeout(scrollTimer.current);
        }
        scrollTimer.current = setTimeout(() => {
          blockSlider.current = false;
        }, 66);
      };
      ref.addEventListener("scroll", handleScroll, false);
      return () => ref.removeEventListener("scroll", handleScroll);
    }
  }, []);

  useEffect(() => {
    if (markerDetails) {
      if (mapSliderState !== SliderState.OPEN) {
        if (contentHeaderRef.current) {
          contentHeaderHeight.current = contentHeaderRef.current.offsetHeight;
        }
        setMapSliderState(SliderState.MEDIUM);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [markerDetails]);

  const handleTouchStart = (e: any) => {
    startY.current = e.touches[0].clientY;
    if (sliderRef.current) {
      scrollTopOnMoveStart.current = sliderRef.current.scrollTop;
    }
  };

  const handleTouchMove = (e: any) => {
    if (!blockSlider.current && imageRef.current) {
      let distY =
        e.touches[0].clientY - startY.current - scrollTopOnMoveStart.current;

      if (Math.abs(distY) > TOUCH_THRESHOLD) {
        distY > 0
          ? (distY = distY - TOUCH_THRESHOLD)
          : (distY = distY + TOUCH_THRESHOLD);

        imageRef.current.style.overflow = "hidden";

        switch (mapSliderState) {
          case SliderState.MEDIUM:
            if (
              distY >= HEIGHT * 0.45 * -1 &&
              distY <= HEIGHT * 0.25 - contentHeaderHeight.current
            ) {
              setMoveY(distY);
              setAnimated(false);
            }
            break;
          case SliderState.OPEN:
            if (
              distY > 0 &&
              distY <= HEIGHT * 0.7 - contentHeaderHeight.current &&
              sliderRef.current &&
              sliderRef.current.scrollTop <= 0
            ) {
              setAnimated(false);
              setMoveY(distY);
            }
            break;
          case SliderState.SMALL:
            if (
              distY < 0 &&
              distY >= (HEIGHT * 0.7 - contentHeaderHeight.current) * -1
            ) {
              setMoveY(distY);
              setAnimated(false);
            }
            break;
        }
      }
    }
  };

  const handleTouchEnd = () => {
    if (imageRef.current) {
      imageRef.current.style.overflow = "";
    }

    scrollTopOnMoveStart.current = 0;
    switch (mapSliderState) {
      case SliderState.MEDIUM:
        if (moveY <= GESTURE_THRESHOLD * -1) {
          setMapSliderState(SliderState.OPEN);
        } else if (moveY >= GESTURE_THRESHOLD) {
          setMapSliderState(SliderState.SMALL);
        } else {
        }
        break;
      case SliderState.OPEN:
        if (moveY > HEIGHT * 0.45) {
          setMapSliderState(SliderState.SMALL);
        } else if (moveY > GESTURE_THRESHOLD) {
          setMapSliderState(SliderState.MEDIUM);
        }
        break;
      case SliderState.SMALL:
        if (moveY < (HEIGHT * 0.25 - contentHeaderHeight.current) * -1) {
          setMapSliderState(SliderState.OPEN);
        } else if (moveY < GESTURE_THRESHOLD * -1) {
          setMapSliderState(SliderState.MEDIUM);
        }
        break;
    }
    setAnimated(true);
    setMoveY(0);
    animationDuration.current = SLIDER_DURATION;
  };

  //move slider to new mapSliderState
  useEffect(() => {
    if (sliderRef.current && imageRef.current) {
      switch (mapSliderState) {
        case SliderState.CLOSE:
          sliderRef.current.style.transform = `translate3d(0,70%,0)`;
          imageRef.current.style.transform = `translate3d(0,100%,0)`;
          if (locateRef.current) {
            locateRef.current.style.transform = `translate3d(0,0,0)`;
          }
          if (menuRef.current) {
            menuRef.current.style.transform = `translate3d(0,0,0)`;
          }
          break;
        case SliderState.MEDIUM:
          sliderRef.current.style.transform = `translate3d(0,45%,0)`;
          imageRef.current.style.transform = `translate3d(0,50%,0)`;
          if (locateRef.current) {
            locateRef.current.style.transform = `translate3d(0,${
              HEIGHT * 0.1 * -1
            }px,0)`;
          }
          if (menuRef.current) {
            menuRef.current.style.transform = `translate3d(0,${
              HEIGHT * 0.1 * -1
            }px,0)`;
          }

          break;
        case SliderState.SMALL:
          sliderRef.current.style.transform = `translate3d(0,calc(70% - ${contentHeaderHeight.current}px),0)`;
          imageRef.current.style.transform = `translate3d(0,100%,0)`;
          break;
        case SliderState.OPEN:
          sliderRef.current.style.transform = `translate3d(0,0%,0)`;
          imageRef.current.style.transform = `translate3d(0,0%,0)`;
          break;
      }
    }
  }, [HEIGHT, animated, locateRef, mapSliderState, menuRef]);

  //transform slider with gesture distance
  useEffect(() => {
    if (sliderRef.current && !animated && imageRef.current) {
      switch (mapSliderState) {
        case SliderState.MEDIUM:
          sliderRef.current.style.transform = `translate3d(0,calc(45% + ${moveY}px),0)`;

          //transform gallery & calculate animation duration after touch end
          if (moveY > 0) {
            const t = interpolateRange(
              0,
              HEIGHT * 0.25 - contentHeaderHeight.current,
              50,
              100,
              moveY
            );
            imageRef.current.style.transform = `translate3d(0,${t}%,0)`;

            animationDuration.current = interpolateRange(
              0,
              HEIGHT * 0.25 - contentHeaderHeight.current,
              SLIDER_DURATION,
              0,
              moveY
            );
          } else {
            const t = interpolateRange(0, HEIGHT * 0.45 * -1, 50, 0, moveY);
            imageRef.current.style.transform = `translate3d(0,${t}%,0)`;

            animationDuration.current = interpolateRange(
              0,
              HEIGHT * 0.45 * -1,
              SLIDER_DURATION,
              0,
              moveY
            );
          }
          break;

        case SliderState.OPEN:
          sliderRef.current.style.transform = `translate3d(0,${moveY}px,0)`;
          //transform gallery
          if (moveY < HEIGHT * 0.45) {
            const t = interpolateRange(0, HEIGHT * 0.45, 0, 50, moveY);
            imageRef.current.style.transform = `translate3d(0,${t}%,0)`;
          } else {
            const t = interpolateRange(
              HEIGHT * 0.45,
              HEIGHT * 0.7 - contentHeaderHeight.current,
              50,
              100,
              moveY
            );
            imageRef.current.style.transform = `translate3d(0,${t}%,0)`;
          }
          animationDuration.current = interpolateRange(
            0,
            HEIGHT * 0.7 - contentHeaderHeight.current,
            SLIDER_DURATION,
            0,
            moveY
          );
          break;

        case SliderState.SMALL:
          sliderRef.current.style.transform = `translate3d(0,calc(70% - ${contentHeaderHeight.current}px + ${moveY}px),0)`;
          //transform gallery
          if (moveY > (HEIGHT * 0.25 - contentHeaderHeight.current) * -1) {
            const t = interpolateRange(
              0,
              (HEIGHT * 0.25 - contentHeaderHeight.current) * -1,
              100,
              50,
              moveY
            );
            imageRef.current.style.transform = `translate3d(0,${t}%,0)`;
          } else {
            const t = interpolateRange(
              (HEIGHT * 0.25 - contentHeaderHeight.current) * -1,
              (HEIGHT * 0.7 - contentHeaderHeight.current) * -1,
              50,
              0,
              moveY
            );
            imageRef.current.style.transform = `translate3d(0,${t}%,0)`;
          }
          animationDuration.current = interpolateRange(
            0,
            (HEIGHT * 0.7 - contentHeaderHeight.current) * -1,
            SLIDER_DURATION,
            0,
            moveY
          );
          break;
      }
    }
  }, [HEIGHT, animated, mapSliderState, moveY]);

  return (
    <Container
      ref={sliderRef}
      mapSliderState={mapSliderState}
      animated={animated}
      animationDuration={animationDuration.current}
    >
      <ImageSlider
        padding={"0"}
        ref={imageRef}
        animated={animated}
        animationDuration={animationDuration.current}
        sliderState={mapSliderState}
        onTouchStart={handleTouchStart}
        onTouchMove={handleTouchMove}
        onTouchEnd={handleTouchEnd}
      >
        {markerDetails?.images.map((image, index) => (
          <SliderImage src={image} key={index} />
        ))}
      </ImageSlider>
      <ContentContainer
        onTouchStart={handleTouchStart}
        onTouchMove={handleTouchMove}
        onTouchEnd={handleTouchEnd}
      >
        <ContentHeader ref={contentHeaderRef}>
          <MemoSwipeIndicator />
          <MemoRating max={5} rating={markerDetails?.rating?.value} />
        </ContentHeader>
        <p>{markerDetails?.description}</p>
        {CommentsMemo}
      </ContentContainer>
    </Container>
  );
}
