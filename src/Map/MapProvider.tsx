import { SearchResult } from "../common/GeoAutocomplete";
import React, {
  ReactNode,
  useState,
  useContext,
  Dispatch,
  SetStateAction
} from "react";
import { Filter, Marker } from "../typings/marker";
import { SliderState } from "./MapSlider";

interface MapContext {
  searchResult?: SearchResult;
  setSearchResult: Dispatch<SetStateAction<SearchResult | undefined>>;
  selectedFilters: Filter[];
  handleFilterClick: (filter: Filter) => void;
  mapSliderState: SliderState;
  setMapSliderState: Dispatch<SetStateAction<SliderState>>;
  selectedMarker?: Marker;
  setSelectedMarker: (marker?: Marker) => void;
  addPlaceLocation?: { lat: number; lng: number };
  setAddPlaceLocation: Dispatch<
    SetStateAction<{ lat: number; lng: number } | undefined>
  >;
}

export const MapContext = React.createContext<MapContext>({
  setSearchResult: () => null,
  searchResult: undefined,
  selectedFilters: [],
  handleFilterClick: () => null,
  mapSliderState: SliderState.CLOSE,
  setMapSliderState: () => null,
  setSelectedMarker: () => null,
  addPlaceLocation: undefined,
  setAddPlaceLocation: () => null
});

type Props = {
  children: ReactNode;
};

export default function MapProvider({ children }: Props) {
  const [searchResult, setSearchResult] = useState<SearchResult>();
  const [selectedFilters, setSelectedFilters] = useState<Filter[]>([]);
  const [selectedMarker, setSelectedMarker] = useState<Marker>();
  const [addPlaceLocation, setAddPlaceLocation] = useState<{
    lat: number;
    lng: number;
  }>();

  const [mapSliderState, setMapSliderState] = useState<SliderState>(
    SliderState.CLOSE
  );

  const setSelectedMarkerHandler = (marker?: Marker) => {
    if (marker && marker === selectedMarker) {
      setMapSliderState(SliderState.MEDIUM);
    }
    setSelectedMarker(marker);
  };

  const handleFilterClick = (filter: Filter) => {
    let data = [...selectedFilters];
    const index = data.findIndex((e) => e.filter === filter.filter);
    if (index === -1) {
      setSelectedFilters([...data, filter]);
    } else {
      data.splice(index, 1);
      setSelectedFilters(data);
    }
  };
  return (
    <MapContext.Provider
      value={{
        setSearchResult,
        searchResult,
        selectedFilters,
        handleFilterClick,
        mapSliderState,
        setMapSliderState,
        selectedMarker,
        setSelectedMarker: setSelectedMarkerHandler,
        addPlaceLocation,
        setAddPlaceLocation
      }}
    >
      {children}
    </MapContext.Provider>
  );
}

export function useMap() {
  return useContext(MapContext);
}
