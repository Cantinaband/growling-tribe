import { useHistory } from "react-router-dom";
import { useEffect, ReactNode, useRef } from "react";
import React from "react";

type Props = {
  children: ReactNode;
};
export default function AppContainer({ children }: Props) {
  const fromExtra = useRef(false);
  const prevent = useRef(false);
  const history = useHistory();
  useEffect(() => {
    const unlisten = history.listen((location, action) => {
      // if (action === "POP") {
      //   if (fromExtra.current && !prevent.current) {
      //     fromExtra.current = false;
      //     prevent.current = true;
      //     history.goBack();
      //     console.log("go back");
      //   } else {
      //     history.push(location.pathname);
      //     fromExtra.current = true;
      //     prevent.current = false;
      //   }
      // }
      // if (action === "PUSH") {
      //   fromExtra.current = false;
      //   prevent.current = false;
      // }
    });

    return () => unlisten();
  }, [history]);

  return <>{children}</>;
}
