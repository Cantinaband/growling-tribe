import { useEffect, useState } from "react";
import { Marker, MarkerDetails } from "../typings/marker";

export default function useMarkerDetails(marker?: Marker) {
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(true);
  const [markerDetails, setMarkerDetails] = useState<MarkerDetails>();

  useEffect(() => {
    if (marker) {
      const unsubscribe = marker.detailsRef.onSnapshot(
        (doc) => {
          const data = { id: doc.id, ...doc.data() } as MarkerDetails;
          setMarkerDetails(data);
        },
        (err) => {
          setLoading(false);
          setError(Boolean(err.message));
        }
      );
      return () => unsubscribe();
    } else {
      setMarkerDetails(undefined);
    }
  }, [marker]);
  return {
    error,
    loading,
    markerDetails
  };
}
