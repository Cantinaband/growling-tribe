import { useState, useEffect } from "react";

export default function usePosition() {
  const [position, setPosition] = useState<{
    latitude?: number;
    longitude?: number;
  }>({});

  const [error, setError] = useState<null | string>(null);

  const onChange = ({ coords }: Position) => {
    setPosition({ latitude: coords.latitude, longitude: coords.longitude });
  };

  const onError = (error: PositionError) => {
    setError(error.message);
  };
  useEffect(() => {
    const geo = navigator.geolocation;

    if (!geo) {
      setError("Geolocation is not supported!");
      return;
    }

    let watcher = geo.watchPosition(onChange, onError);

    return () => geo.clearWatch(watcher);
  }, []);
  return { ...position, error };
}
