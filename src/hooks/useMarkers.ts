import { useEffect, useState } from "react";
import { getMarkers } from "../firebase";
import { Marker, Filter } from "../typings/marker";

export default function useMarkers(filters?: Filter[]) {
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(true);
  const [markers, setMarkers] = useState<Marker[]>();

  useEffect(() => {
    const unsubscribe = getMarkers(filters).onSnapshot(
      (doc) => {
        const data = doc.docs.map((doc) => {
          return {
            ...doc.data()
          } as Marker;
        });
        setMarkers(data);
      },
      (err) => {
        setLoading(false);
        setError(Boolean(err.message));
      }
    );
    return () => unsubscribe();
  }, [filters]);
  return {
    error,
    loading,
    markers
  };
}
