import { useState, useEffect } from "react";
import { getFilters } from "../firebase";

type Filter = {
  filter: string;
};

export default function useFilters() {
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(true);
  const [filters, setFilters] = useState<Filter[]>();

  useEffect(() => {
    const unsubscribe = getFilters().onSnapshot(
      (doc) => {
        const data = doc.docs.map((doc) => {
          return {
            ...doc.data()
          } as Filter;
        });
        setFilters(data);
      },
      (err) => {
        setLoading(false);
        setError(Boolean(err.message));
      }
    );
    return () => unsubscribe();
  }, []);
  return {
    error,
    loading,
    filters
  };
}
