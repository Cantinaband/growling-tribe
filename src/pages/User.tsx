import Page from "../common/Page";
import React from "react";
import Button from "../common/Button";
import { useUser } from "../provider/UserProvider";
import { useHistory } from "react-router-dom";

export default function User() {
  const { signOut } = useUser();
  const history = useHistory();
  return (
    <Page padded>
      <Button
        onClick={() => {
          signOut().then(() => {
            history.push("/");
          });
        }}
      >
        logout
      </Button>
    </Page>
  );
}
