import React from "react";
import MapContainer from "../Map/MapContainer";
import styled from "styled-components";
import Nav from "../common/Nav";
import GeoAutocomplete from "../common/GeoAutocomplete";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  overflow: hidden;
`;
export default function AddPlaceLocation() {
  return (
    <Container>
      <Nav>
        <GeoAutocomplete />
      </Nav>
      <MapContainer addPlaceLocationMode />
    </Container>
  );
}
