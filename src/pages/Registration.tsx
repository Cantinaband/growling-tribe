import Page from "../common/Page";
import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { useForm } from "react-hook-form";
import { useUser } from "../provider/UserProvider";
import { useHistory } from "react-router-dom";
import TextField from "../common/TextField";
import Button from "../common/Button";
const Form = styled.form`
  display: flex;
  flex-direction: column;
  margin-bottom: 16px;
`;
type LoginData = {
  email?: string;
  password?: string;
};
export default function Registration() {
  const [pending, setPending] = useState(false);
  const { register, handleSubmit } = useForm<LoginData>();
  const { user, signUp } = useUser();
  const history = useHistory();

  useEffect(() => {
    if (user) {
      history.push("/");
    }
  }, [history, user]);

  const onSubmit = handleSubmit(({ email, password }) => {
    setPending(true);
    signUp(email!, password!)
      .then(() => {
        setPending(false);
      })
      .catch(() => {
        setPending(false);
      });
  });

  return (
    <Page padded>
      <Form onSubmit={onSubmit}>
        <TextField
          type="email"
          placeholder="Email..."
          ref={register({ required: true })}
          name="email"
        />
        <TextField
          type="password"
          placeholder="Password..."
          ref={register({ required: true })}
          name="password"
        />
        <Button disabled={pending} type="submit">
          Sign up
        </Button>
      </Form>
    </Page>
  );
}
