import Page from "../common/Page";
import React, { useState, useEffect, useRef } from "react";
import { useForm } from "react-hook-form";
import TextArea from "../common/TextArea";
import styled from "styled-components";
import FileField from "../common/FileField";
import HorizontalSlider from "../common/HorizontalSlider";
import RoundClose from "../common/RoundClose";
import Button from "../common/Button";
import { useHistory } from "react-router-dom";
import { Filter as FilterProp } from "../typings/marker";
import { addMarker, addFilter } from "../firebase";
import FullScreenLoading from "../common/FullScreenLoading";
import { useMap } from "../Map/MapProvider";
import firebase from "firebase";
import TextField from "../common/TextField";
import useFilters from "../hooks/useFilters";

const Form = styled.form`
  display: flex;
  flex-direction: column;
  margin-bottom: 16px;
`;

const AddImages = styled(FileField)`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  margin: 0 auto;
  border: 2px solid #000;
  box-sizing: border-box;
  width: 20vw;
  height: 20vw;
  border-radius: 100%;
  padding: 16px;
`;
const ImageSlider = styled(HorizontalSlider)`
  height: 30vh;
  margin: 32px 0;
`;

const ImageContainer = styled.div`
  position: relative;
  margin-right: 16px;
`;

const SliderImage = styled.img`
  width: 65vw;
  object-fit: cover;
  height: 100%;
`;

const Filters = styled(HorizontalSlider)`
  background: #f6f6f6;
  margin-bottom: 32px;
  box-sizing: border-box;
`;

const Filter = styled.div<{ selected: boolean }>`
  display: flex;
  flex: 1 0 auto;
  align-items: center;
  justify-content: center;
  padding: 8px 16px;
  border-radius: 100px;
  margin-right: 16px;
  border: 1px solid rgb(0, 0, 0, 0.12);
  background: ${(p) => (p.selected ? "#FF9C00" : "#f6f6f6")};
`;

const StyledTextArea = styled(TextArea)`
  margin: 20px 16px;
`;

const ImageDelete = styled(RoundClose)`
  position: absolute;
  top: 1rem;
  right: 1rem;
`;

const StyledButton = styled(Button)`
  margin: 32px 16px;
`;

const FilterInput = styled.input<{ ref?: any }>`
  background: transparent;
  font-size: 16;
  width: 100%;
  outline: none;
  border: none;
  border-bottom: 2px solid rgba(0, 0, 0, 0.5);
  padding: 4px 0;
  margin-right: 8px;
  border-radius: 0;
  :focus,
  :not(:placeholder-shown) {
    border-bottom: 2px solid rgba(0, 0, 0, 1);
  }
  :-webkit-autofill,
  :-webkit-autofill:hover,
  :-webkit-autofill:focus,
  :-webkit-autofill:active {
    box-shadow: 0 0 0 30px white inset !important;
  }
`;

type FormData = {
  description?: string;
};

export default function AddPlace() {
  const history = useHistory();
  const { register, handleSubmit } = useForm<FormData>();
  const [images, setImages] = useState<File[]>([]);
  const [categorys, setCategorys] = useState<FilterProp[]>([]);
  const [pending, setPending] = useState(false);
  const { addPlaceLocation, setAddPlaceLocation } = useMap();
  const filterInput = useRef<HTMLInputElement>(null);
  const [newFilter, setNewFilter] = useState("");
  const { filters } = useFilters();
  useEffect(() => {
    if (!addPlaceLocation) {
      history.push("/");
    }
  }, [addPlaceLocation, history]);

  const onSubmit = handleSubmit(async ({ description }) => {
    setPending(true);
    await addMarker({
      category: categorys!,
      description: description!,
      images,
      geopoint: new firebase.firestore.GeoPoint(
        addPlaceLocation!.lat,
        addPlaceLocation!.lng
      )
    })
      .then(() => {
        setPending(false);
        setAddPlaceLocation(undefined);
      })
      .catch(() => setPending(false));
  });

  const removeImage = (index: number) => {
    let data = [...images];
    data.splice(index, 1);
    setImages(data);
  };

  if (!addPlaceLocation) {
    return null;
  }

  const handleFilterClick = (filter: FilterProp) => {
    let data = [...categorys];
    if (!categorys?.some((f) => f.filter === filter.filter)) {
      setCategorys([...data, filter]);
    } else {
      setCategorys(data.filter((f) => f.filter !== filter.filter));
    }
  };

  const addNewFilter = async () => {
    await addFilter(newFilter).then(() => {
      setNewFilter("");
      if (filterInput.current) {
        filterInput.current.value = "";
      }
    });
  };

  const isValid = () => {
    return !categorys.length;
  };

  return (
    <Page>
      <Form onSubmit={onSubmit}>
        <Filters padding="16px">
          {filters?.map((e, i) => (
            <Filter
              key={i}
              onClick={() => handleFilterClick(e)}
              selected={!!categorys?.some((f) => f.filter === e.filter)}
            >
              {e.filter}
            </Filter>
          ))}
          <Filter selected={false}>
            <FilterInput
              ref={filterInput}
              placeholder="Filter..."
              type="text"
              onChange={(e) => setNewFilter(e.target.value)}
            />
            <Button small onClick={addNewFilter}>
              add
            </Button>
          </Filter>
        </Filters>
        <StyledTextArea
          placeholder="Description..."
          name="description"
          ref={register({ required: true })}
        />
        {!!images.length && (
          <ImageSlider padding="0 16px">
            {images.map((e, index) => {
              const url = URL.createObjectURL(e);
              return (
                <ImageContainer key={index}>
                  <SliderImage src={url} />
                  <ImageDelete onClick={() => removeImage(index)} />
                </ImageContainer>
              );
            })}
          </ImageSlider>
        )}
        <AddImages
          name="images"
          onChange={(files) => {
            setImages((prev) => [...prev, ...files]);
          }}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 46 35"
          >
            <path
              stroke="#000"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M34.5 34H45V8.096H34.5S34.5 5.5 31 3 22.5.5 19 3s-3.5 5.096-3.5 5.096h-4s.189.186-.088-.596c-.782-2.209-5.218-2.209-6 0-.276.782.088.596.088.596H1V34h14.5"
            />
            <circle
              cx="25.5"
              cy="22.5"
              r="11.5"
              stroke="#000"
              strokeWidth="2"
            />
            <path
              fill="#000"
              d="M16.035 22.802a1 1 0 001.96.396l-1.96-.396zm13.607-6.386a1 1 0 00.759-1.85l-.76 1.85zm-11.646 6.782c.569-2.811 1.602-4.56 2.752-5.648 1.149-1.087 2.49-1.585 3.804-1.76 1.324-.178 2.602-.023 3.564.184a11.669 11.669 0 011.439.409l.072.027.015.006h.001l.378-.925a88.224 88.224 0 00.378-.926l-.004-.002-.01-.003a4.435 4.435 0 00-.136-.053 13.69 13.69 0 00-1.711-.488c-1.101-.238-2.625-.43-4.252-.212-1.637.219-3.397.857-4.912 2.29-1.514 1.432-2.707 3.586-3.339 6.704l1.96.398z"
            />
          </svg>
        </AddImages>
        <StyledButton type="submit" disabled={isValid()}>
          save
        </StyledButton>
      </Form>
      {pending && <FullScreenLoading />}
    </Page>
  );
}
