import React, { useRef } from "react";
import styled from "styled-components";
import Header from "../common/Header";
import FloatingMenu from "../common/FloatingMenu";
import Map from "../Map";
import { BOTTOM_BUTTON_DISTANCE } from "../Map/MapContainer";
import { SLIDER_DURATION } from "../Map/MapSlider";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  overflow: hidden;
`;

export const FloatingMenuContainer = styled.div`
  position: absolute;
  bottom: ${BOTTOM_BUTTON_DISTANCE};
  right: 16px;
  z-index: 1;
  transition: transform ${SLIDER_DURATION}ms ease-out;
`;

export default function Home() {
  const floatingMenuRef = useRef<HTMLDivElement>(null);

  return (
    <Container>
      <Header />
      <FloatingMenuContainer ref={floatingMenuRef}>
        <FloatingMenu />
      </FloatingMenuContainer>
      <Map menuRef={floatingMenuRef} />
    </Container>
  );
}
