import Page from "../common/Page";
import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import TextField from "../common/TextField";
import styled from "styled-components";
import Button from "../common/Button";
import { useUser } from "../provider/UserProvider";
import { useHistory } from "react-router-dom";
import Link from "../common/Link";

const Form = styled.form`
  display: flex;
  flex-direction: column;
  margin-bottom: 16px;
`;

type LoginData = {
  email?: string;
  password?: string;
};

export default function Login() {
  const [pending, setPending] = useState(false);
  const { register, handleSubmit } = useForm<LoginData>();
  const { user, login } = useUser();
  const history = useHistory();

  useEffect(() => {
    if (user) {
      history.push("/");
    }
  }, [history, user]);

  const onSubmit = handleSubmit(({ email, password }) => {
    setPending(true);
    login(email!, password!)
      .then(() => {
        setPending(false);
      })
      .catch(() => {
        setPending(false);
      });
  });

  return (
    <Page padded>
      <Form onSubmit={onSubmit}>
        <TextField
          type="email"
          placeholder="Email..."
          ref={register({ required: true })}
          name="email"
        />
        <TextField
          type="password"
          placeholder="Password..."
          ref={register({ required: true })}
          name="password"
        />
        <Button disabled={pending} type="submit">
          Login
        </Button>
      </Form>
      <Link to="/registration">No Account? Sign up here</Link>
    </Page>
  );
}
