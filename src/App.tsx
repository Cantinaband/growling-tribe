import React from "react";

import { createGlobalStyle } from "styled-components";
import Home from "./pages/Home";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Login from "./pages/Login";
import UserProvider from "./provider/UserProvider";
import Registration from "./pages/Registration";
import User from "./pages/User";
import AddPlace from "./pages/AddPlace";
import AddPlaceLocation from "./pages/AddPlaceLocation";
import AppContainer from "./AppContainer";
import AuthenticatedRoute from "./AuthenticatedRoute";
import MapProvider from "./Map/MapProvider";

const GlobalStyle = createGlobalStyle`
  html {
    margin: 0;
  }
  body {
    position: fixed;
    top: 0;
    left: 0; 
    right: 0;
    bottom: 0;
    margin: 0;
    font-family: Arial, Helvetica, sans-serif;
    overflow: hidden
  }
  #root {
    position: fixed;
    top: 0;
    left: 0; 
    right: 0;
    bottom: 0;
  }
`;

function App() {
  return (
    <Router basename={process.env.PUBLIC_URL}>
      <AppContainer>
        <UserProvider>
          <MapProvider>
            <GlobalStyle />

            <Switch>
              <Route path="/" exact>
                <Home />
              </Route>
              <Route path="/add/info" component={AddPlace}></Route>
              <AuthenticatedRoute path="/add" exact>
                <AddPlaceLocation />
              </AuthenticatedRoute>
              <Route path="/user/">
                <User />
              </Route>
              <Route path="/login/">
                <Login />
              </Route>
              <Route path="/registration/">
                <Registration />
              </Route>
            </Switch>
          </MapProvider>
        </UserProvider>
      </AppContainer>
    </Router>
  );
}

export default App;
