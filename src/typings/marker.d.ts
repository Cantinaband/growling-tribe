export interface MarkerDetails {
  id: string;
  geopoint: firebase.firestore.GeoPoint;
  description: string;
  category: Filter[];
  images: string[];
  comments?: Comment[];
  rating?: Rating;
}
export type Filter = {
  filter: name;
};
export type Marker = {
  detailsRef: firebase.firestore.DocumentReference;
  geopoint: firebase.firestore.GeoPoint;
  category: Filter[];
};

type Rating = {
  value: number;
  numberOfRatings: number;
};

export type Comment = {
  message: string;
  rating: number;
  createdAt: firebase.firestore.Timestamp;
  userId: string;
};

export interface AddMarker extends MarkerDetails {
  id?: string;
  images: File[];
}
