import React from "react";
import { RouteProps, Route } from "react-router-dom";
import { useUser } from "./provider/UserProvider";
import Login from "./pages/Login";

export default function AuthenticatedRoute({
  component,
  children,
  ...rest
}: RouteProps) {
  const { user } = useUser();
  return (
    <Route
      component={component && user ? component : component ? Login : undefined}
      {...rest}
    >
      {children && user ? children : children ? <Login /> : null}
    </Route>
  );
}
